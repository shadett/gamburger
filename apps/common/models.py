# coding=utf-8

from __future__ import unicode_literals
from django.core.exceptions import AppRegistryNotReady
from django.db import models
from django.apps import apps
from django.db.utils import ProgrammingError
from django.utils.lru_cache import lru_cache


class MetaSetting(models.base.ModelBase):
    """
    For easy to use settings(Setting model).
    Instead of Setting.objects.get(name='KEY'), you can use Setting.KEY
    """
    @lru_cache()
    def __getattr__(self, key):
        val = None
        try:
            _model = apps.get_model('common', 'Option')
            try:
                val = _model.objects.get(name=key).value
            except _model.DoesNotExist:
                val = None
            except ProgrammingError:
                val = None

        except AppRegistryNotReady:
            pass

        if isinstance(val, basestring):
            val = val.encode('utf-8')

        return val

    def set_value(self, key, value, value_type=unicode):

        """
        key = key,
        value = value,
        value_type accept following values:
                unicode
                int
                float

        value_type default - basestring
        """

        _model = apps.get_model('common', 'Option')

        if _model.objects.filter(name=key).exists():
            _model.objects.filter(name=key).update(val=value)

        else:
            _model.objects.create(name=key, val=value,
                                  value_type=Option.VALUE_TYPE_MAPPING.get(value_type))


class Option(models.Model):
    """
    Simple project settings
    """

    __metaclass__ = MetaSetting

    class Meta:
        verbose_name = 'Настройки'
        verbose_name_plural = 'Настройки'
        app_label = 'common'

    VALUE_TYPE_CHOICES = (
        (1, 'Строка',),
        (2, 'Целое',),
        (3, 'Вещественное',),
        (4, 'Rich text',),
        # (4, u'Булево',),
    )

    VALUE_TYPE_MAPPING = {float: 3, int: 2, unicode: 1}

    VALUE_TYPE_CONVERTER = {
        1: lambda x: unicode(x),
        2: lambda x: int(x),
        3: lambda x: float(x),
        4: lambda x: unicode(x),
    }

    name = models.CharField('Ключ', max_length=100, unique=True)
    val = models.TextField('Значение')
    value_type = models.PositiveSmallIntegerField(u'Тип значения', choices=VALUE_TYPE_CHOICES)
    desc = models.TextField('Описание')
    ts = models.DateTimeField('Дата изменения', auto_now=True)

    @property
    def value(self):
        return self.VALUE_TYPE_CONVERTER[self.value_type](self.val)

    @staticmethod
    def get_options():
        options = {o.name: o.val for o in Option.objects.all()}
        return options