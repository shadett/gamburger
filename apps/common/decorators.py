# coding=utf-8
from traceback import print_exc
import datetime
import os
import sys
from django.http.response import HttpResponse
from django.template.context import RequestContext
from django.template.loader import render_to_string
from django.utils.six import wraps
import json as sjson

from django.conf import settings

__author__ = 'shade'


def render_to_template(template):
    def wrap(view):
        @wraps(view)
        def response(request, *args, **kwargs):
            context = view(request, *args, **kwargs)

            if isinstance(context, HttpResponse):
                return context
            else:

                if request.method == 'GET' and request.GET.get('raw', '0') == '1':
                    return HttpResponse(render_to_string(template, context), content_type='text/plain')

                response = HttpResponse(render_to_string(template, context, context_instance=RequestContext(request)), status=context.get('STATUS', None))

                expires = datetime.datetime.now() + datetime.timedelta(days=100)

                for cook in context.get('__COOKIES', []):
                    if 'key' in cook and 'value' in cook:
                        response.set_cookie(cook['key'], cook['value'], expires=expires, path='/')

                return response

        return response

    return wrap


def json(f):

    dthandler = lambda obj: obj.isoformat() if isinstance(obj, datetime.datetime) else None

    @wraps(f)
    def _decorator(request, *args, **kwargs):
        try:
            data = f(request, *args, **kwargs)

            if isinstance(data, HttpResponse):
                response = data
            else:
                json_content = sjson.dumps(data, ensure_ascii=False, default=dthandler)

                if isinstance(json_content, unicode):
                    json_content = json_content.encode('utf-8')
                response = HttpResponse(json_content, 'application/json; charset=UTF-8')

        except Exception, e:
            print_exc()

            json_content = sjson.dumps({"error": unicode(e)}, ensure_ascii=False, default=dthandler)

            if isinstance(json_content, unicode):
                json_content = json_content.encode('utf-8')

            response = HttpResponse(json_content.encode('utf-8'), 'application/json; charset=UTF-8')

        return response

    return _decorator


def pid_lock(*args, **kwargs):

    def outer_wrapper(f):
        filename = kwargs.get('filename')

        def inner_wrapper(*args, **kwargs):
            pid = str(os.getpid())
            pidfile = settings.BASE_DIR + "/%s.pid" % filename

            if os.path.isfile(pidfile):
                sys.exit()
            else:
                file(pidfile, 'w').write(pid)
            try:
                f(*args, **kwargs)
            except:
                pass
            os.unlink(pidfile)

        return inner_wrapper

    return outer_wrapper

