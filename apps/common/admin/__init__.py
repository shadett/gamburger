from django.contrib import admin
from apps.common.admin.models import OptionAdmin
from apps.common.models import Option

__author__ = 'aleksey'

admin.site.register(Option, OptionAdmin)