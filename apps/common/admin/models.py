
from django import forms
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget
from apps.common.models import Option

__author__ = 'aleksey'


class OptionAdminForm(forms.ModelForm):
    val = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Option
        exclude = ()


class OptionAdmin(admin.ModelAdmin):
    list_display = ('name', 'value_type', 'val', 'desc')
    list_editable = ('val', 'desc')
    fieldsets = (None, {'fields': ('name', 'value_type', 'val', 'desc')}),

    def get_form(self, request, obj=None, **kwargs):
        if obj and obj.value_type == 4:
            return OptionAdminForm
        return super(OptionAdmin, self).get_form(request, obj, **kwargs)