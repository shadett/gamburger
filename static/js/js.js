$(document).ready(function() {
	var sl_for = $('.slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		fade: true,
		adaptiveHeight:true,
		asNavFor: '.slider-nav'
	});
	$('.slider-for img').on('click',function(){
		sl_for.slick('slickNext');
	});
	$('.slider-nav').slick({
		slidesToShow: 5,
		slidesToScroll: 5,
		asNavFor: '.slider-for',
		dots: false,
		arrows: true,
		centerMode: true,
		focusOnSelect: true
	});
});