from django.contrib import admin
from django.contrib.auth.models import Group
from gamburger.models import News, Gallery

admin.site.register(Gallery)
admin.site.register(News)
admin.site.unregister(Group)