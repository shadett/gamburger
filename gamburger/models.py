# coding=utf-8

import os
import uuid
from ckeditor.fields import RichTextField

from django.db import models
from imagekit.models.fields import ImageSpecField
from pilkit.processors.resize import SmartResize

__author__ = 'aleksey'


def chunked_path(instance, filename):

    """
    make path from filename for FileField upload_to
    example: blabla/f0/a5/64/5e/ee/df/f0a5645eeedf.jpg
    """

    extension = filename.split('.')[-1]

    filename = uuid.uuid4().hex

    fname = map(None, *([iter(filename)] * 2))

    if len(fname) % 2:
        fname[-1] = (fname[-1][0], '')

    fname = map(lambda x: ''.join(x), fname)

    fname.insert(0, instance.__class__.__name__.lower())
    fname.append('%s.%s' % (filename, extension))

    return os.path.join(*fname)


class Gallery(models.Model):
    class Meta:
            verbose_name = u'Галлерея'
            verbose_name_plural = u'Галлереи'
            app_label = 'gamburger'

    TYPE_GALLERY = (
        (1, u'Графика'),
        (2, u'Живопись'),
        (3, u'Иллюстрации'),
    )

    name = models.CharField(u'Имя', max_length=200)
    img = models.ImageField(verbose_name=u'Изображение', upload_to=chunked_path)
    type_gallery = models.SmallIntegerField(u'Тип галлерии', choices=TYPE_GALLERY)
    img_150x200 = ImageSpecField([SmartResize(150, 200)], source='img', format='JPEG')

    def __unicode__(self):
        return self.name


class News(models.Model):
    class Meta:
            verbose_name = u'Новости'
            verbose_name_plural = u'Новости'
            app_label = 'gamburger'

    title = models.CharField(u'Заголовок', max_length=200)
    text = RichTextField(u'Текст')
    created = models.DateTimeField(u'Дата добавления', auto_now_add=True)

    def __unicode__(self):
        return self.title