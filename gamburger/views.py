# coding=utf-8

from apps.common.decorators import render_to_template
from apps.common.models import Option
from gamburger.models import Gallery, News

__author__ = 'aleksey'


@render_to_template('index.html')
def index(request):

    return {}


@render_to_template('about.html')
def about(request):
    return {'about': Option.about or '',
            'text': Option.text or '',
            'phone': Option.phone or '',
            'email': Option.email or '',
            }


@render_to_template('news.html')
def news(request):
    qs = News.objects.order_by('-created')
    return {'news': qs}


@render_to_template('gallery.html')
def gallery(request, type):
    qs = Gallery.objects.filter(type_gallery=type)
    return {'gallery_qs': qs}